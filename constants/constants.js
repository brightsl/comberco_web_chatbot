module.exports = {
  // bot generator type
  SHOPPING_TYPE: "shpt",

  // bot connected type
  FB_BOT_TYPE: "fbt",
  LINE_BOT_TYPE: "linebt",

  // package type
  FREE_TRIAL_7: "ft7",
  PKG_1M: "pkg1",
  PKG_3M: "pkg3",
  PKG_6M: "pkg6",
  PKG_12M: "pk12",

  // date format
  DATE_FORMAT: "YYYY-MM-DDDD HH:ii:ss",

  // bot UI type
  QUICK_REPLY: "qr",
  CAROUSEL: "cr",

  // AnswerActionSchema.type
  TXT_ANSWER_TYPE: "tat",
  SERVICE_TYPE: "st",

  // AnswerActionSchema.service_action
  SHOW_PRODUCTS_IN_SHOP: "spis",
  SHOW_PRODUCTS_IN_CART: "spic",
  ADD_PRODUCT_TO_CART: "aptc",
  DELETE_PRODUCT_IN_CART: "dpic",
  EDIT_PRODUCT_IN_CART: "epic",
  CHECK_OUT: "co",
  SHOW_PAY_CHANNEL: "spc",
  LIST_MENU: 'lim',
  INFORM_PAYMENT: 'infp',
  CHECK_ORDER: 'chkord',
}