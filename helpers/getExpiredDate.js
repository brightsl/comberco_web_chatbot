require("../constants/constants")

module.exports = function (start_date, package_type) {
  switch (package_type) {
    case PKG_1M:
      return moment(start_date).add(30, 'days').format(DATE_FORMAT)
    case PKG_3M:
      return moment(start_date).add(90, 'days').format(DATE_FORMAT)
    case PKG_6M:
      return moment(start_date).add(180, 'days').format(DATE_FORMAT)
    case PKG_12M:
      return moment(start_date).add(360, 'days').format(DATE_FORMAT)
    default:
      return -1
  }
}