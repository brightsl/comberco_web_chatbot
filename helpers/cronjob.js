const Bot = require("../models/bot.model")
const moment = require('moment');
// const mongoose = require('mongoose');

async function batchUpdateBot(){
  var start = moment().startOf('day'); // set to 12:00 am today
  var end = moment().endOf('day'); // set to 23:59 pm today
  console.log(start,"|",end)
  await Bot.find({end_date:{$lt:end}},function(err,result){
    if (err) throw err;
    // console.log("batchupdatebot:",result)
    result.forEach(function(element) {
      console.log(element._id,"| state:",element.state);
      if (element.state == "online"){
        Bot.updateOne({_id:element._id}, 
          { 
            $set: {
              state: "offline"
            },
          }, function (err) {
          if (err) throw err;
          console.log("success");
        });
      }
    });
  });
  
}
module.exports = function () {
  batchUpdateBot()    
}
