const User = require("../models/user.model");

module.exports = async function(req, res, next) {
  await User.findById(req.session.passport.user, function(err, user) {
    // console.log("helper req:",req)
    if (err || !user) return res.redirect("../users/showlogin");    
    return user;
  });
};

