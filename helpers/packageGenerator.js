const constants = require("../constants/constants")
const get_expired_date = require("../helpers/getExpiredDate")
const Bot = require("../models/bot.model")
const Package = require("../models/package.model")
const moment = require('moment');
const mongoose = require("mongoose");

const findBot = async function (params) { 
  return await Bot.findOne(params)  
}
const findPackage = async function (params) { 
  return await Package.findOne(params)  
}
async function addbot(package,user, bot_type, bot_info, product_list, package_type) {
  let newBot = new Bot({
    type: bot_type,
    user_id: user,
    product_list: product_list,
    package_id: package_type,
    package_type: package.package_name,
    free_trial: true,
    start_date: moment().format("YYYY-MM-DD"),
    end_date: moment().add(parseInt(package.package_term), 'days').format("YYYY-MM-DD")
  })
  newBot.product_list = product_list
  if (bot_type == constants.FB_BOT_TYPE) {
    newBot.page_fb_id = bot_info.page_fb_id
    newBot.page_fb_token = bot_info.page_fb_token
    newBot.page_fb_name = bot_info.page_fb_name
  }
  return await newBot.save()
}

function genShoppingBot(user, bot_type, bot_info, product_list, package_type) {
  console.log("gen shopping bot:")
  console.log("bot_info:",bot_info)
  botquery = {
    user_id: mongoose.Types.ObjectId(user),
    page_fb_id: bot_info.page_fb_id
  };
  packagequery = {_id:mongoose.Types.ObjectId(package_type)};
  let mybot = findBot(botquery)
  mybot.then(function(bot){
    if(bot != null){
      if (bot_type == constants.FB_BOT_TYPE) {
        Bot.updateOne(botquery, 
        { 
          $set: {
            page_fb_id: bot_info.page_fb_id,
            page_fb_token: bot_info.page_fb_token,
            page_fb_name : bot_info.page_fb_name,
          },
        }, function (err) {
          if (err) throw err;
          console.log('Bot successfully updated.');
          }
        );
      }
    } else {
      let myPackage = findPackage(packagequery);
      myPackage.then(function(package){
        console.log("package:",package)
        let newBot = new Bot({
          type: bot_type,
          user_id: user,
          product_list: product_list,
          package_id: package_type,
          // package_type: package.package_name,
          free_trial: true,
          start_date: moment().format("YYYY-MM-DD"),
          end_date: moment().add(parseInt(package.package_term), 'days').format("YYYY-MM-DD")
        })
        newBot.product_list = product_list
        if (bot_type == constants.FB_BOT_TYPE) {
          newBot.page_fb_id = bot_info.page_fb_id
          newBot.page_fb_token = bot_info.page_fb_token
          newBot.page_fb_name = bot_info.page_fb_name
        }
        newBot.save(function(err) {
        // newbot = addbot(package,user, bot_type, bot_info, product_list, package_type)
        // newbot.then(function(err){
          if (err) throw err;
          console.log('Bot successfully created.');
        })
      })     
    }
  })
  // Bot.findOne({
  //   user_id: mongoose.Types.ObjectId(user),
  //   page_fb_id: bot_info.page_fb_id
  // }, function (err, bot) {
  //   console.log("bot:",bot)
  //   if (bot != null) {
  //     if (bot_type == constants.FB_BOT_TYPE) {
  //       bot.page_fb_id = bot_info.page_fb_id
  //       bot.page_fb_token = bot_info.page_fb_token
  //       bot.page_fb_name = bot_info.page_fb_name
  //       bot.save(function (err, oldBot) {
  //         err ? false : true
  //       })
  //     }
  //   } else {
    //   Package.findById(package_type, function (err, package) {
    //     let newBot = new Bot({
    //       type: bot_type,
    //       user_id: user,
    //       product_list: product_list,
    //       package_id: package_type,
    //       free_trial: true,
    //       start_date: moment().format("YYYY-MM-DDDD HH:ii:ss"),
    //       end_date: moment().add(parseInt(package.package_term), 'days').format("YYYY-MM-DDDD HH:ii:ss")
    //     })
    //     newBot.product_list = product_list
    //     if (bot_type == constants.FB_BOT_TYPE) {
    //       newBot.page_fb_id = bot_info.page_fb_id
    //       newBot.page_fb_token = bot_info.page_fb_token
    //       newBot.page_fb_name = bot_info.page_fb_name
    //     }

    //     newBot.save(function (err, newBot) {
    //       err ? false : true
    //     });
    //   })
    // }
  // })

}


module.exports = function (gen_type, user, bot_type, bot_info = {
  page_fb_token: "",
  page_fb_id: 0,
  page_fb_name: "",
}, product_list, package_type) {
  console.log("gentype:",gen_type,"==",constants.SHOPPING_TYPE)
  if (gen_type == constants.SHOPPING_TYPE)
    return genShoppingBot(user, bot_type, bot_info, product_list, package_type)
}
