var express = require("express");
var router = express.Router();

// middlewares
var auth = require("../middlewares/auth");

// helpers
var is_role = require("../middlewares/check_role");

//models
const mongoose = require("mongoose");
const Account = require("../models/account.model"),
  redirect_err = "/error",
  redirect_ok = "/account/"
const User = require("../models/user.model");


router.use([auth()]);

// routes
router.get("/", index);

// router.use([is_role('admin')]);
router.get("/create", create);
router.post("/create", store);
router.get("/edit/:id", edit)
router.post("/edit/:id", put)
router.get("/delete/:id", remove);

// MongoDB Query
const findUser = async function (params) { 
  return await User.findOne(params)  
}

async function getallaccount() {
  return await Account.find({}).populate("user_id").exec()
}

async function getUseraccount(params) {
  return await Account.find(params)
}

// show all bank [view]
function index(req, res) {
  var query = {
    _id: mongoose.Types.ObjectId(req.session.passport.user)
  };
  // console.log("query : ",query)
  let myuser = findUser(query);
  
  myuser.then(function(result) {
    if (result.role == 'admin'){
        console.log("get all account")
        result = getallaccount();
        result.then(function(myresult){
        console.log("All account: ",myresult)                
            res.render("pages/account", {
                title: "Account List",
                account: myresult,
            });
        })
    }
    else{
        var accountquery = {
            user_id: mongoose.Types.ObjectId(req.session.passport.user)
        };
        result = getUseraccount(accountquery);
        result.then(function(myresult){
            res.render("pages/account", {
                title: "Account List",
                account: myresult,
            });                   
        })        
    }
 });
  return
}

// create a new account [view]
function create(req, res) {
  res.render("form/reg_account", {
      title: "Create a New Bank Account",
  });
}

// store a new account
function store(req, res) {
  // let user = get_user();
    // console.log("user_id:",req.session.passport.user)
  Account.create(Object.assign(req.body, { 
      user_id: req.session.passport.user,
      bank_name: req.body.bank,
      account_id: req.body.accountid,
      account_name:req.body.accountname,
      }),function (err) {
      if (err) res.redirect(redirect_err)
      res.redirect(redirect_ok);
  });
}

// edit account [view]
function edit(req, res) {
  Account.findOne({_id:req.params.id}, function (err,result) {
    if (err || !result) res.redirect(redirect_err);      
    res.render("edit/edit_reg_account", {
      title: "Edit Account",
      account: result,
    });    
  });
}

// edit account
function put(req, res) {
  Account.updateOne({_id:req.params.id}, 
    { 
      $set: {
        bank_name: req.body.bank,
        account_id: req.body.accountid,
        account_name:req.body.accountname,
      },
    }, function (err) {
    if (err) res.redirect(redirect_err);
    res.redirect(redirect_ok);
  });
}

function remove(req, res) {
  var query = {
    _id: mongoose.Types.ObjectId(req.params.id)
  };
  Account.deleteOne(query, function(err) {
    if (err) res.redirect(redirect_err);
    res.redirect(redirect_ok);
  });
}


module.exports = router;


