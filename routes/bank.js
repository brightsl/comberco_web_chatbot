var express = require("express");
var router = express.Router();
const multer = require('multer');
const fs = require('fs')

// middlewares
var auth = require("../middlewares/auth");

// helpers
var is_role = require("../middlewares/check_role");

const mongoose = require("mongoose");
const Bank = require("../models/bank.model"),
  redirect_err = "../",
  redirect_ok = "../bank/"
const User = require("../models/user.model");

//Upload file
const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './bank_logo/');
  },
  filename: function(req, file, cb) {
    cb(null, new Date().toISOString() + file.originalname);
  }
});
const fileFilter = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};
const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

router.use([auth()]);

// routes
router.get("/", index);

router.use([is_role('admin')]);
router.get("/create", create);
router.post("/create",upload.single('thefile'), store);
router.get("/edit/:id", edit)
router.post("/edit/:id",upload.single('thefile'), put)
router.get("/delete/:id", remove);

// get Condition
const findUser = async function (params) { 
  return await User.findOne(params)  
}

// show all bank [view]
function index(req, res) {
  var query = {
    _id: mongoose.Types.ObjectId(req.session.passport.user)
  };
  // console.log("query : ",query)
  let myuser = findUser(query);
  myuser.then(function(result) {
    if (result.role == 'admin'){
      console.log("get all bank")
      result = getallbank();
      result.then(function(myresult){
        console.log("All bank: ",myresult)
        res.render("pages/bank", {
          title: "Bank List",
          bank: myresult,
        });
      })
    }
    else{
      res.redirect("../../")
    }
 })
  return
}
async function getallbank() {
  return await Bank.find({})
}

// create a new package [view]
function create(req, res) {
  res.render("form/reg_bank", {
    title: "Create a New Bank"
  });
}

// store a new package
function store(req, res) {
  console.log("file: ",req.file)
  Bank.create(Object.assign(req.body, { 
      bank_name: req.body.name,
      bank_logo:req.file.path,
      }),function (err) {
      if (err) res.redirect(redirect_err)
      res.redirect(redirect_ok);
  });
}

// edit package [view]
function edit(req, res) {
  Bank.findOne({_id:req.params.id}, function (err,result) {
    if (err || !result) res.redirect(redirect_err);
    console.log("bank:",result)
    res.render("edit/edit_reg_bank", {
      title: "Edit Package",
      bank: result
    });
  });
}

// edit package
function put(req, res) {
  if (req.file) {
    fs.unlink(req.body.oldfile, (err) => {
      console.log("in unlinked")
      if (err) {
        console.error(err)
        return
      }
    })
    Bank.updateOne({_id:req.params.id}, 
      { 
        $set: {
          bank_name: req.body.name,
          bank_logo:req.file.path,
        },
      }, function (err) {
      if (err) res.redirect("../../");
      res.redirect("../");
    });
  } else {
    Bank.updateOne({_id:req.params.id}, 
      { 
        $set: {
          bank_name: req.body.name,
        },
      }, function (err) {
      if (err) res.redirect("../../");
      res.redirect("../");
    });
  }
}

function remove(req, res) {
  var query = {
    _id: mongoose.Types.ObjectId(req.params.id)
  };
  Bank.findOne(query, function (err,result) {
    if (err || !result) res.redirect(redirect_err);
    console.log("bank:",result)
    fs.unlink(result.bank_logo, (err) => {
      if (err) {
        console.error(err)
        return
      }
    })
    Bank.deleteOne(query, function(err) {
    
      if (err) res.redirect("../../");
      res.redirect("../");
    });
  });
  
}


module.exports = router;


