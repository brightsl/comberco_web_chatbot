var express = require("express");
var passport = require("passport");
const request = require("request");
const mongoose = require("mongoose");

const Bot = require("../models/bot.model");
const User = require("../models/user.model");
var getUserInfo = require("../helpers/getUserInfo.js");
require("../config/passport")(passport);
var package_generator = require("../helpers/packageGenerator");
var router = express.Router();
var auth = require("../middlewares/auth.js");
const constants = require("../constants/constants");
/* GET home page. */
router.get("/", [auth()], index);
router.get("/addaccount", addaccount);

var pageData = [];
var userToken = "";
var userObjectId = "";

// FACEBOOK ROUTES
router.get(
  "/auth/facebook",
  // "/auth/facebook/token",
  passport.authenticate("facebook", {
  // passport.authenticate('facebook-token', {
    scope: [
      "public_profile",
      "email",
    ],
  })
);

 // "manage_pages",
        // "pages_show_list",
      // "pages_messaging"
// router.get('/auth/facebook/subscriptions',
//   passport.authenticate('facebook', {
//     authType: 'rerequest',
//     scope: [
//       "manage_pages",
//       "pages_show_list",
//       "pages_messaging",
//       "pages_messaging_subscriptions",
//     ]
//   }));
    
router.get('/auth/facebook/subscriptions',
  passport.authenticate('facebook', {
  // passport.authenticate('facebook-token', {
    authType: 'rerequest',
    scope: [
        "manage_pages",
        "pages_messaging",
        // "pages_messaging_subscriptions",
        "pages_show_list",
        "publish_pages"
    ],
  }));

router.get(
  "/auth/facebook/callback",
  passport.authenticate("facebook", {
  // passport.authenticate('facebook-token', {
    failureRedirect: "/users/showlogin"
  }),
  function (req, res) {
    // Successful authentication, redirect home.
    res.redirect("/");
  }
);

router.get("/fb_bot_subscription", [auth()], function (req, res) {
  // res.redirect('/');

  console.log("fb_bot_subscription")
  console.log(req.query, "  |  ", req.session.passport.user)
  let data = botSubScriptions(req.query, req.session.passport.user, function (
    respond
  ) {
    //do 
    res.redirect("/");

  });
});

router.post("/post_page", [auth()], function (req, res) {
  // res.redirect('/');
  
  // request(
  //   `https://graph.facebook.com/v3.3/${req.query.page_id}/feed?message=Welcome to my Bot!&access_token=${req.query.access_token}`,
  //   {
    console.log("req.query:",req.body)
    request.post(`https://graph.facebook.com/v3.3/${req.body.page_id}/feed?message=${req.body.message}&access_token=${req.body.access_token}`,
    {
      json: true
    },
    (err, res, body) => {
      if (err) {
        return err;
      }

      
      console.log("body:", body)
      // return respond(body.data);
    }
  );
  res.redirect("/");
});

// route for logging out
router.get("/logout", function (req, res) {
  req.logout();
  res.redirect("/");
});

function index(req, res) {
  User.findById(req.session.passport.user, function (err, user) {
    if (err) res.redirect("/");
    console.log("userdata:",user)
    // getFacebookPageData(user.fb_token, function (respond) {
    getFacebookPageData(user, function (respond) {
      res.render("pages/index", {
        title: "Dashboard",
        // user: req.session,
        user:user,
        pageData: respond,
        // myuser: req.session.passport.user
      });
    });
  });
}

function addaccount(req, res) {
  res.render("form/reg_account", {
    title: "Add Service"
  });
}

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {
  // if user is authenticated in the session, carry on
  if (req.isAuthenticated()) return next();

  // if they aren't redirect them to the home page
  res.redirect("/");
}

function getFacebookPageData(req, respond) {
  // let token="EAAexNEJeSaMBANLYxKIvUA6wzMyazZBQfH8rSDbePk7gUFxcf9P4ayaj2WEiDfBk3VZCfKIlVY0p9yZCFrRsAp4tZCTvi4lBl7ZAi7nEVmq6LfOrFBH6JJ7EuAbfZAvWre7xiuIbo9QkFp4FAmul7puBpICIf9wq54Ve1EJRdzPQZDZD"
  request(
    `https://graph.facebook.com/v3.3/${req.fb_id}/accounts?access_token=${req.fb_token}`,
    // `https://graph.facebook.com/v3.3/me/accounts?access_token=${token}`,
    {
      json: true
    },
    (err, res, body) => {
      if (err) {
        return err;
      }

      pageData = body.data;
      console.log("pageData:", pageData)
      return respond(body.data);
    }
  );
}

function botSubScriptions(req, user_id, respond) {

  console.log("req:", req.page_id, req.access_token)
  // console.log(req)
  console.log("user_id:", user_id)

  let fb_req = request(`https://graph.facebook.com/${req.page_id}?fields=access_token&access_token=${req.access_token}`,
    // let fb_req = request(`https://graph.facebook.com/${req.page_id}?fields=access_token&access_token=${token}`,
    { json: true }, (err, res, body) => {
      console.log("body:", body)
      if (err) {
        return err;
      }

      var formData = {
        // subscribed_fields: JSON.stringify(["messages", "messaging_postbacks"]),
        subscribed_fields: JSON.stringify(["messages", "messaging_postbacks"]),
        access_token: req.access_token
        // access_token: token
      };
      console.log("subscribed:", formData.subscribed_fields)
      console.log("accesstoken:", formData.access_token)
      console.log("bodyid:", req.page_id)

      //save page_id,page_access_token


      request.post(
        {
          url: `https://graph.facebook.com/v3.3/${req.page_id}/subscribed_apps`,
          formData: formData
        },
        async function optionalCallback(
          subscribedErr,
          subscribedRes,
          subscribedAppsBody
        ) {
          if (subscribedErr) {
            return console.error("upload failed:", subscribedErr);
          }

          // console.timeStamp("package_generator");

          console.log("package_generator start")
          await package_generator(
            constants.SHOPPING_TYPE,
            user_id,
            constants.FB_BOT_TYPE,
            {
              page_fb_id: req.page_id,
              // page_fb_token: req.access_token,
              page_fb_token: req.access_token,
              page_fb_name: req.page_name
            },
            [],
            "5d6fed5e77aac5449692f3af"
          );
          console.log("package_generator end")

          // console.timeStamp("package_generator");
          console.log("subscribedAppsBody:", subscribedAppsBody)

          respond(subscribedAppsBody);
        }
      );

      return res.body;
    }
  );
}

module.exports = router;
