var express = require("express");
var router = express.Router();

// middlewares
var auth = require("../middlewares/auth.js");

// helpers
// var get_user = require("../helpers/getUserInfo");

const mongoose = require("mongoose");
const Question = require("../models/question.model"),
  redirect_err = "/error",
  redirect_ok = "/question/"
const User = require("../models/user.model");

// all routes middlewares
router.use([auth()]);

// routes
router.get("/", index);
router.get("/create", create);
router.post("/create", store);
router.get("/edit/:id", edit)
router.post("/edit/:id", put)
router.get("/delete/:id", remove);

// MongoDB Query
const findUser = async function (params) { 
  return await User.findOne(params)  
}
async function getallquestion() {
  return await Question.find({}).populate("user_id").exec()
}
async function getquestion(params) {
  return await Question.find(params)
}

// show all question [view]
function index(req, res) {
  var query = {
    _id: mongoose.Types.ObjectId(req.session.passport.user)
  };
  // console.log("query : ",query)
  let myuser = findUser(query);
  myuser.then(function(result) {
    if (result.role == 'admin'){
      // console.log("get all questions")
      result = getallquestion();
      result.then(function(myresult){
        console.log("All questions: ",myresult)
        res.render("pages/question", {
          title: "Question List",
          question: myresult,
        });
      })
    }
    else{
      console.log("get user question")
      result = getquestion({user_id:req.session.passport.user});
      result.then(function(myresult){
        console.log("user questions: ",myresult)
        res.render("pages/question", {
          title: "Question List",
          question: myresult,
        });
      })
    }
 })
  return
}

// create a new question [view]
function create(req, res) {
  res.render("form/reg_question", {
    title: "Create a new question"
  });
}

// store a new question
function store(req, res) {
  Question.create(Object.assign(req.body, { 
    user_id: req.session.passport.user,
    question:req.body.question,
    answer:req.body.answer,}),
    function (err) {
      if (err) res.redirect(redirect_err)
      res.redirect(redirect_ok);
  });
}

// edit question [view]
function edit(req, res) {
  Question.findOne({_id:req.params.id}, function (err,result) {
    if (err || !result) res.redirect(redirect_err);
    console.log("questions:",result)
    res.render("edit/edit_reg_question", {
      title: "Edit Question",
      question: result
    });
  });
}

// edit question
function put(req, res) {
  Question.updateOne({_id:req.params.id}, 
    { 
      $set: {
        user_id: req.session.passport.user,
        question:req.body.question,
        answer:req.body.answer
      },
    }, function (err) {
    if (err) res.redirect(redirect_err);
    res.redirect(redirect_ok);
  });
}

// remove a question
function remove(req, res) {
  var query = {
    _id: mongoose.Types.ObjectId(req.params.id)
  };
  Question.deleteOne(query, function(err) {
    if (err) res.redirect(redirect_err);
    res.redirect(redirect_ok);
  });
}

module.exports = router;
