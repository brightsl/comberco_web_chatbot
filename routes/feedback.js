var express = require("express");
var router = express.Router();

// middlewares
var auth = require("../middlewares/auth");

// helpers
var is_role = require("../middlewares/check_role");

const mongoose = require("mongoose");
const Feedback = require("../models/feedback.model"),
  redirect_err = "/error",
  redirect_ok = "/feedback/"
// const User = require("../models/user.model");

router.use([auth()]);

// routes
router.get("/", [is_role('admin')],index);

// router.use([is_role('admin')]);
router.get("/create", create);
router.post("/create", store);
router.get("/edit/:id", [is_role('admin')],edit)
router.post("/edit/:id", [is_role('admin')],put)
router.get("/delete/:id", [is_role('admin')],remove);

// MongoDB Query
// const findUser = async function (params) { 
//   return await User.findOne(params)  
// }
async function getallFeedback() {
  return await Feedback.find({}).populate("user_id").exec()
}

// show all packages [view]
function index(req, res) {
//   var query = {
//     _id: mongoose.Types.ObjectId(req.session.passport.user)
//   };
//   let myuser = findUser(query);
//   myuser.then(function(result) {
    // if (result.role == 'admin'){
    //   console.log("get all feedback")
      result = getallFeedback();
      result.then(function(myresult){
        res.render("pages/feedback", {
          title: "Feedback List",
          feedback: myresult,
        });
      })
    // }
    // else{
    //   res.redirect(redirect_err)
    // }
//  })
//   return
}


// create a new package [view]
function create(req, res) {
  res.render("form/reg_feedback", {
    title: "Create a new package"
  });
}

// store a new package
function store(req, res) {
  // let user = get_user();
  Feedback.create(Object.assign(req.body, { 
        user_id:req.session.passport.user,
        feedback: req.body.feedback,
        description:req.body.description,
        status:"open"}),function (err) {
    if (err) res.redirect(redirect_err)
    res.redirect("/");
  });
}

// edit package [view]
function edit(req, res) {
  Feedback.findOne({_id:req.params.id}, function (err,result) {
    if (err || !result) res.redirect(redirect_err);
    res.render("edit/edit_reg_feedback", {
      title: "Edit Feedback",
      feedback: result
    });
  });
}

// edit package
function put(req, res) {
  Feedback.updateOne({_id:req.params.id}, 
    { 
      $set: {
        feedback: req.body.feedback,
        description:req.body.description,
        status: req.body.status
      },
    }, function (err) {
    if (err) res.redirect(redirect_err);
    res.redirect(redirect_ok);
  });
}

function remove(req, res) {
  var query = {
    _id: mongoose.Types.ObjectId(req.params.id)
  };
  Feedback.deleteOne(query, function(err) {
    if (err) res.redirect(redirect_err);
    res.redirect(redirect_ok);
  });
}


module.exports = router;


