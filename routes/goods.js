var express = require("express");
var router = express.Router();
const multer = require('multer');
const fs = require('fs')

// middlewares
var auth = require("../middlewares/auth.js");

// helpers
// var get_user = require("../helpers/getUserInfo");

const mongoose = require("mongoose");
const Goods = require("../models/good.model"),
  redirect_err = "/error",
  redirect_ok = "/goods/"
const User = require("../models/user.model");

//Upload file
const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './goods_pic/');
  },
  filename: function(req, file, cb) {
    cb(null, new Date().toISOString() + file.originalname);
  }
});
const fileFilter = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};
const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

// all routes middlewares
router.use([auth()]);

// routes
router.get("/", index);
router.get("/create", create);
router.post("/create",upload.single('goodspic'), store);
router.get("/edit/:id", edit)
router.post("/edit/:id",upload.single('goodspic'), put)
router.get("/delete/:id", remove);

// MongoDB Query
const findUser = async function (params) { 
  return await User.findOne(params)  
}
async function getallgoods() {
  return await Goods.find({}).populate("user_id").exec()
}
async function getgoods(params) {
  return await Goods.find(params)
}

// show all goods [view]
function index(req, res) {
  var query = {
    _id: mongoose.Types.ObjectId(req.session.passport.user)
  };
  // console.log("query : ",query)
  let myuser = findUser(query);
  myuser.then(function(result) {
    if (result.role == 'admin'){
      console.log("get all goods")
      result = getallgoods();
      result.then(function(myresult){
        console.log("All goods: ",myresult)
        res.render("pages/goods", {
          title: "Goods List",
          goods: myresult,
        });
      })
    }
    else{
      console.log("get user user")
      result = getgoods({user_id:req.session.passport.user});
      result.then(function(myresult){
        console.log("user goods: ",myresult)
        res.render("pages/goods", {
          title: "Goods List",
          goods: myresult,
        });
      })
    }
 })
  return
}

// create a new goods [view]
function create(req, res) {
  res.render("form/reg_goods", {
    title: "Create a new goods"
  });
}

// store a new goods
function store(req, res) {
  Goods.create(Object.assign(req.body, { 
    user_id: req.session.passport.user,
    goods_name:req.body.name,
    goods_description:req.body.description,
    goods_price: req.body.price,
    goods_pic:req.file.path}),
    function (err) {
      if (err) res.redirect(redirect_err)
      res.redirect(redirect_ok);
  });
}

// edit goods [view]
function edit(req, res) {
  Goods.findOne({_id:req.params.id}, function (err,result) {
    if (err || !result) res.redirect(redirect_err);
    console.log("goods:",result)
    res.render("edit/edit_reg_goods", {
      title: "Edit Goods",
      goods: result
    });
  });
}

// edit goods
function put(req, res) {
  if (req.file) {
    fs.unlink(req.body.oldpic, (err) => {
      console.log("in unlinked")
      if (err) {
        console.error(err)
        return
      }
    })
    Goods.updateOne({_id:req.params.id}, 
      { 
        $set: {
          user_id: req.session.passport.user,
          goods_name:req.body.name,
          goods_description:req.body.description,
          goods_price: req.body.price,
          goods_pic:req.file.path
        },
      }, function (err) {
      if (err) res.redirect(redirect_err);
      res.redirect(redirect_ok);
    });
  } else {
    Goods.updateOne({_id:req.params.id}, 
      { 
        $set: {
          user_id: req.session.passport.user,
          goods_name:req.body.name,
          goods_description:req.body.description,
          goods_price: req.body.price,
        },
      }, function (err) {
      if (err) res.redirect(redirect_err);
      res.redirect(redirect_ok);
    });
  }
}

// remove a goods
function remove(req, res) {
  var query = {
    _id: mongoose.Types.ObjectId(req.params.id)
  };
  Goods.findOne(query, function (err,result) {
    if (err || !result) res.redirect(redirect_err);
    console.log("goods:",result)
    fs.unlink(result.goods_pic, (err) => {
      if (err) {
        console.error(err)
        return
      }
    })
    Goods.deleteOne(query, function(err) {    
      if (err) res.redirect(redirect_err);
      res.redirect(redirect_ok);
    });
  });
}
module.exports = router;
