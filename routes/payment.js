var express = require("express");
var router = express.Router();
var moment = require('moment');
const multer = require('multer');
const fs = require('fs')

// middlewares
var auth = require("../middlewares/auth");

// helpers
var get_user = require("../helpers/getUserInfo");
var is_role = require("../middlewares/check_role");

//models
const mongoose = require("mongoose");
const Payment = require("../models/payment.model"),
  redirect_err = "/error",
  redirect_ok = "/payment/"
const User = require("../models/user.model");
const Bot = require("../models/bot.model");
const Package = require("../models/package.model");

//Upload file
const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './payment_evidence/');
  },
  filename: function(req, file, cb) {
    cb(null, new Date().toISOString() + file.originalname);
  }
});
const fileFilter = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};
const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

// use middlewares
router.use([auth()]);

// routes
router.get("/", index);

// router.use([is_role("user")]);

router.get("/create", create);
router.post("/create", store);
router.get("/edit/:id", edit)
router.post("/edit/:id", upload.single('evidence'),put)

// MongoDB Query
const findUser = async function (params) { 
  return await User.findOne(params)  
}
async function getBot(params){
  return await Bot.find(params)
}
async function getallBot(){
  return await Bot.find({})
}
async function getallPackage(){
  return await Package.find({})
}
async function getPackage(params){
  return await Package.find(params)
}
async function joinadminPayment(){
  return await Payment.aggregate([
    {
      $lookup:
      {
          from: 'bots',
          localField: 'bot_id',
          foreignField: '_id',
          as: 'botdata'        
      }
    },
    {
      $lookup:
      {
          from: 'packages',
          localField: 'package_id',
          foreignField: '_id',
          as: 'packagedata'        
      }
    },
    {
      $lookup:
      {
          from: 'users',
          localField: 'user_id',
          foreignField: '_id',
          as: 'userdata'        
      }
    }
  ])
}
async function joinuserPayment(params){
  return await Payment.aggregate([
    {
        $match: params
    }, 
    {
      $lookup:
      {
          from: 'bots',
          localField: 'bot_id',
          foreignField: '_id',
          as: 'botdata'        
      }
    },
    {
      $lookup:
      {
          from: 'packages',
          localField: 'package_id',
          foreignField: '_id',
          as: 'packagedata'        
      }
    },
  ])
}
async function getPayment(params){
  return await Payment.find(params)
}
async function regPayment(params){
  return await Payment.create(Object.assign(params, { 
    user_id: req.session.passport.user,
      bot_id: params.bot_id,
      package_id: params.package_id,
      evidence_transfer:"",}
    )
  )
}
async function updateBotEnddate(botid,enddate,package_id){
  await Bot.updateOne({_id:botid}, 
    { 
      $set: {
        package_id: package_id,
        end_date:enddate,
        state:"online",
      },
    }, function (err) {
    if (err) throw err
  });
}
async function updatePaymentstatus(params,res){
  await Payment.updateOne({_id:params.id}, 
    { 
      $set: {
        status:params.status,
        confirmedAt: moment().format("YYYY-MM-DD HH:mm:ss")
      },
    }, function (err) {
    if (err) res.redirect(redirect_err);
    res.redirect(redirect_ok);
  });
}
async function updatePayment(params,res){
  await Payment.find({_id:params.id}).populate("bot_id").populate("package_id").exec(function(err,result){
    if (err || !result) res.redirect(redirect_err);
    console.log("result: ",result)
    var end_date = result[0].bot_id.end_date
    if(params.oldstatus != params.status){
      if (params.oldstatus == "confirmed"){
        end_date = moment(result[0].bot_id.end_date,"DD-MM-YYYY").subtract(result[0].package_id.package_term,'days')
      }else if (params.status == "confirmed"){
        end_date = moment(result[0].bot_id.end_date,"DD-MM-YYYY").add(result[0].package_id.package_term,'days')
      }
    }
    console.log("end_date:",end_date)
    let updatebot = updateBotEnddate(result[0].bot_id._id,end_date,result[0].package_id._id)
    updatebot.then(function(){
      updatePaymentstatus(params,res)  
    })
  })
}


// show all payments [view]
function index(req, res) {
  var query = {
    _id: mongoose.Types.ObjectId(req.session.passport.user)
  };
  // console.log("query : ",query)
  let myuser = findUser(query);
  
  myuser.then(function(result) {
    if (result.role == 'admin'){
        console.log("get all payment")
        result = joinadminPayment();
        result.then(function(myresult){
          console.log("All payment: ",myresult)  
          // console.log(myresult)              
          res.render("pages/payment", {
              title: "Payment List",
              payment: myresult,
              moment:moment
          });
        })
    }
    else{
      console.log("user payment")
      console.log(req.session.passport.user)
      var paymentquery = {
          user_id: mongoose.Types.ObjectId(req.session.passport.user)
      };
      result = joinuserPayment(paymentquery);
      result.then(function(myresult){
        console.log("User payment: ",myresult)  
        res.render("pages/payment", {
            title: "Payment",
            payment: myresult,
            moment:moment
        });                   
      })        
    }
 });
  return
}

// create a new payment [view]
function create(req, res) {
  var query = {
    _id: mongoose.Types.ObjectId(req.session.passport.user)
  };
  // console.log("query : ",query)
  let myuser = findUser(query);
  
  myuser.then(function(result) {
    if (result.role === "admin"){
      let result = getallBot();
      result.then(function(myresult){
        console.log("Bot : ",myresult)
        let pack = getallPackage()
        pack.then(function(mypackage){
          console.log("Package : ",mypackage)
          res.render("form/reg_payment", {
            title: "Create a New Payment",
            bot: myresult,
            package:mypackage,
          });
        })      
      });
    } 
    else
    {
      let result = getBot({user_id:req.session.passport.user});
      result.then(function(myresult){
        console.log("Bot : ",myresult)
        let pack = getallPackage()
        pack.then(function(mypackage){
          console.log("Package : ",mypackage)
          res.render("form/reg_payment", {
            title: "Create a New Payment",
            bot: myresult,
            package:mypackage,
          });
        })      
      });
    }
  });  
}

// store a new payment
function store(req, res) {
  console.log("myuser:",req.body)
    Payment.create(Object.assign(req.body, { 
      user_id: req.session.passport.user,
      bot_id: req.body.bot_id,
      package_id: req.body.package_id,
      evidence_transfer:"",}),function (err) {
        if (err) res.redirect(redirect_err);
        res.redirect(redirect_ok);
    });
}

// function store2(req,res){
//   let regpay = regPayment(req.body)
//   regpay.then(function(err){
//     console.log("reg err:",err)
//     let getpack = getPackage({_id:req.body.package_id})
//     getpack.then(function(result){
//       let botdata = getBot({_id:req.body.bot_id})
//       botdata.then(function(myresult){
//         let end_datenow = moment(myresult.end_date,'DD/MM/YYYY');
//         let end_datenew = moment(end_datenow).add(result.package_term,'days');
//         Bot.updateOne({_id:req.body.bot_id}, 
//           { 
//               $set: {
//                 end_date:end_datenew,
//               },
//             }, function (err) {
//             if (err) res.redirect("../../");
//             res.redirect("../");
//           });
//       })      
//     })
//   })
// }

// edit payment [view]
function edit(req, res) {
  let result = getBot({user_id:req.session.passport.user});
  result.then(function(myresult){
    console.log("Bot : ",myresult)
    let pack = getallPackage()
    pack.then(function(mypackage){
      console.log("Package : ",mypackage)
      let pay = getPayment({_id:req.params.id})
      pay.then(function(lastresult){
        console.log("payment:",lastresult)
        res.render("edit/edit_reg_payment", {
          title: "Edit Payment",
          bot: myresult,
          package:mypackage,
          payment:lastresult,
        });
      })        
    })      
  });         
}

// edit a payment
function put(req, res) {
  console.log(req.file)
  console.log(req.body)
  if (req.body.bot_id){
    if (req.file) {
      fs.unlink(req.body.oldevidence, (err) => {
        console.log("in unlinked")
        if (err) {
          console.error(err)
          return
        }
      })
      Payment.updateOne({_id:req.params.id}, 
      { 
          $set: {
            bot_id: req.body.bot_id,
            package_id: req.body.package_id,
            status:req.body.status,
            evidence_transfer:req.file.path,
          },
        }, function (err) {
        if (err) res.redirect(redirect_err);
        res.redirect(redirect_ok);
      });
    } else {
      Payment.updateOne({_id:req.params.id}, 
        { 
          $set: {
            bot_id: req.body.bot_id,
            package_id: req.body.package_id,
            status:req.body.status,
          },
        }, function (err) {
        if (err) res.redirect(redirect_err);
        res.redirect(redirect_ok);
      });
    }
  } else {
      updatePayment(req.body,res)    
  }
}

module.exports = router;
