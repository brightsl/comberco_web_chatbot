var express = require("express");
var router = express.Router();
const moment = require('moment');

// middlewares
var auth = require("../middlewares/auth");

// helpers
var is_role = require("../middlewares/check_role");

//models
const mongoose = require("mongoose");
const Bot = require("../models/bot.model"),
  redirect_err = "/error",
  redirect_ok = "/bot/"
const Goods = require("../models/good.model");
const Account = require("../models/account.model");
const User = require("../models/user.model");

router.use([auth()]);

// routes
router.get("/", index);

// router.use([is_role('admin')]);
// router.get("/create", create);
// router.post("/create", store);
router.get("/edit/:id", edit)
router.post("/edit/:id", put)
router.get("/adminedit/:id", adminedit)
router.post("/adminedit/:id", adminput)
router.get("/delete/:id/:goods_id", remove);
router.get("/delete/:id/1/:account_id", removeacc);
router.get("/details/:id", details);

// MongoDB Query
const findUser = async function (params) { 
  return await User.findOne(params)  
}
async function joinallBot(){
  return await Bot.aggregate([
  {
    $lookup:
    {
      from: 'packages',
      localField: 'package_id',
      foreignField: '_id',
      as: 'packagedata'      
    },
  },
  {
    $lookup:
    {
      from: 'users',
      localField: 'user_id',
      foreignField: '_id',
      as: 'userdata'          
    }
  },
  ])
}
async function joinuserBot(params){
  return await Bot.aggregate([
  {
      $match: params
  }, 
  {
    $lookup:
    {
      from: 'packages',
      localField: 'package_id',
      foreignField: '_id',
      as: 'packagedata'      
    }
  },
  ])
}
async function getUserbot(params){
  return await Bot.find(params)
}
async function getUsergoods(params){
  return await Goods.find(params)
}
async function getUserAccount(params){
  return await Account.find(params)
}
// async function getBotwithGoods(params){
//   return await Bot.findById(params).populate('goods_list')
// }


//show bot details
function details(req, res) {
  Bot.findById(req.params.id).populate('goods_list').exec(function(err, result) {
    console.log(result)
    res.render("pages/bot_detail", {
        title: "Bot Detail",
        bot: result,
    });     
  })        
}

// show all bot [view]
function index(req, res) {
  var query = {
    _id: mongoose.Types.ObjectId(req.session.passport.user)
  };
  // console.log("query : ",query)
  let myuser = findUser(query);
  
  myuser.then(function(result) {
    if (result.role == 'admin'){
        console.log("get all bot")
        result = joinallBot();
        result.then(function(myresult){
        console.log("All bot: ",myresult)                
            res.render("pages/bot", {
                title: "Bot List",
                bot: myresult,
                moment:moment,
            });
        })
    }
    else{
        var query = {
            user_id: mongoose.Types.ObjectId(req.session.passport.user)
        };
        console.log("get user bot")
        result = joinuserBot(query);
        result.then(function(myresult){
          // console.log("bot:",myresult)
            res.render("pages/bot", {
                title: "Bot List",
                bot: myresult,
                moment:moment,
            });                   
        })        
    }
 });
  return
}


// edit account [view]
function edit(req, res) {
  // let mybot = getBotwithGoods({_id:req.params.id})
  Bot.findById(req.params.id).populate('goods_list').populate('account_channels').exec(function(err, result) {
      if (err || !result) res.redirect(redirect_err);
      let allgoods = getUsergoods({user_id:result.user_id});
      allgoods.then(function(goods){
          let allaccount = getUserAccount({user_id:result.user_id})
          allaccount.then(function(account){       
            res.render("edit/edit_reg_bot", {
              title: "Edit Bots",
              bot: result,
              goods:   goods,
              account: account,
            });
          })          
      });
    });
}

// edit account
function put(req, res) {  
  console.log("put:",req.body)
  if (req.body.goods_id){
    console.log("goods_id")
    Bot.updateOne({_id:req.params.id}, 
    { 
      $push: {
        goods_list: req.body.goods_id,      
      },
    }, function (err) {
    if (err) res.redirect(redirect_err);
    res.redirect(redirect_ok);
    });  
  }else {
    console.log("account_id")
    Bot.updateOne({_id:req.params.id}, 
      { 
        $push: {
          account_channels: req.body.account_id,
        },
      }, function (err) {
      if (err) res.redirect(redirect_err);
      res.redirect(redirect_ok);
      });  
  }
}

function remove(req, res) {
  Bot.updateOne({_id:req.params.id}, 
    { 
      $pull: {
        goods_list: req.params.goods_id,
      },
    }, function (err) {
    console.log("remove goods finished")
    if (err) res.redirect(redirect_err);
    res.redirect(redirect_ok);
    });  
}

function removeacc(req, res) {
  // console.log("log:",req.params)
  Bot.updateOne({_id:req.params.id}, 
    { 
      $pull: {
        account_channels: req.params.account_id,
      },
    }, function (err) {
    console.log("remove account finished")
    if (err) res.redirect(redirect_err);
    res.redirect(redirect_ok);
    });  
}

function adminedit(req, res) {
  // let mybot = getBotwithGoods({_id:req.params.id})
  Bot.findById(req.params.id).exec(function(err, result) {
      if (err || !result) res.redirect(redirect_err);
      res.render("edit/edit_reg_bot_admin", {
        title: "Edit Bots Admin",
        bot: result,
      });
    })          
}

// edit account
function adminput(req, res) {  
  Bot.updateOne({_id:req.params.id}, 
  { 
    $set: {
      state: req.body.state,
    },
  }, function (err) {
  if (err) res.redirect(redirect_err);
  res.redirect(redirect_ok);
  });  
  
}


module.exports = router;


