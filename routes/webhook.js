"use strict";

//
// Imports dependencies and set up http server

const request = require("request"),
  express = require("express"),
  body_parser = require("body-parser"),
  mongoose = require("mongoose"),
  UserState = require("../models/state_user_messages.model"),
  Bot = require("../models/bot.model"),
  Goods = require("../models/good.model"),
  CustomerOrder = require("../models/customer_order.model"),
  User = require("../models/user.model"),
  Account = require("../models/account.model"),
  Bank = require("../models/bank.model"),
  GoodsConfirmation = require("../models/goods_confirmation.model"),
  Question = require("../models/question.model")

var router = express.Router();

//   app = express().use(body_parser.json()); // creates express http server

//get reply token
//find match message

// Accepts POST requests at /webhook endpoint
router.post("/", (req, res) => {
  console.log("POST webhook");
  // Parse the request body from the POST
  let body = req.body;

  // let bot = Bot.find({ page_fb_id: 'small' });

  // Check the webhook event is from a Page subscription
  if (body.object === "page") {
    body.entry.forEach(function (entry) {
      // Gets the body of the webhook event
      let page_fb_id = entry.id;
      let webhook_event = entry.messaging[0];

      // Get the sender PSID
      let sender_psid = webhook_event.sender.id;
      // Check if the event is a message or postback and
      // pass the event to the appropriate handler function

      UserState.findOne({
        page_fb_id: page_fb_id,
        sender_id: sender_psid
      }, function (err, userState) {
        if (userState) {
          let last_time = Date.parse(userState.updatedAt)
          let timestamp = webhook_event.timestamp
          // 3600 => 1 hours
          //console.log("LIFE_TIME", timestamp / 1000, " ", last_time / 1000, timestamp / 1000 - last_time / 1000)
          if (parseInt(timestamp / 1000) - parseInt(last_time / 1000) >= 300) {
            Bot.findOne({
              page_fb_id: page_fb_id,
              state: "online"
            }, function (err, bot) {
              if (bot && !err) {
                delete_state(sender_psid, page_fb_id, bot.page_fb_token, "คุณไม่ได้ติดต่อบอทภายมากกว่า 5 นาที โปรดทำรายการใหม่")
              }
            })
          }
          else {
            if (webhook_event.message) {
              handleMessage(sender_psid, webhook_event.message, page_fb_id);
            } else if (webhook_event.postback) {
              handlePostback(sender_psid, webhook_event.postback, page_fb_id);
            }
          }
        }
        else {
          //console.log("PASSS")
          if (webhook_event.message) {
            handleMessage(sender_psid, webhook_event.message, page_fb_id);
          } else if (webhook_event.postback) {
            handlePostback(sender_psid, webhook_event.postback, page_fb_id);
          }
        }
      })

    });
    // Return a '200 OK' response to all events
    res.status(200).send("EVENT_RECEIVED");
  } else {
    // Return a '404 Not Found' if event is not from a page subscription
    res.sendStatus(404);
  }
});

// Accepts GET requests at the /webhook endpoint
router.get("/", (req, res) => {
  /** UPDATE YOUR VERIFY TOKEN **/
  const VERIFY_TOKEN = "Arh0Oqf0oN60Vj5gezZa";

  // Parse params from the webhook verification request
  let mode = req.query["hub.mode"];
  let token = req.query["hub.verify_token"];
  let challenge = req.query["hub.challenge"];

  // Check if a token and mode were sent
  if (mode && token) {
    // Check the mode and token sent are correct
    if (mode === "subscribe" && token === VERIFY_TOKEN) {
      // Respond with 200 OK and challenge token from the request
      //console.log("WEBHOOK_VERIFIED");
      //console.log('PAGE_ACCESS_TOKEN :' + PAGE_ACCESS_TOKEN)
      //console.log("VERIFY_TOKEN :" + VERIFY_TOKEN);

      res.status(200).send(challenge);
    } else {
      // Responds with '403 Forbidden' if verify tokens do not match
      res.sendStatus(403);
    }
  }
});

async function updateState(page_fb_id, sender_id, state, tmp = null) {
  let update = {
    current_stage: state
  }
  if (tmp) Object.assign(update, tmp)

  await UserState.findOneAndUpdate(
    {
      page_fb_id: page_fb_id,
      sender_id: sender_id
    },
    {
      $set: update
    },
    {
      upsert: true,
      useFindAndModify: false
    },
    function (err, userState) {
      if (err) return false;
      return true;
    }
  );

}

function getMainMenu() {
  return {
    title: "โปรดเลือกเมนูด้านล่าง",
    quick_replies: [

      {
        content_type: "text",
        title: "สั่งซื้อสินค้า",
        payload: "BUY_ORDER"
      },
      {
        content_type: "text",
        title: "แจ้งชำระเงิน",
        payload: "INFORM_PAYMENT"
      },
      {
        content_type: "text",
        title: "วิธีชำระเงิน",
        payload: "SHOW_PAY_CHANNELS"
      },
      {
        content_type: "text",
        title: "คำถาม",
        payload: "SHOW_QUESTIONS"
      }
      // {
      //   content_type: "text",
      //   title: "ตรวจสอบสินค้า",
      //   payload: "CHECK_ORDER"
      // }
    ]

  };
}

function confirm_cur_order(sender_psid, page_fb_id, page_fb_token, bot_id, user_id) {
  UserState.findOne(
    {
      sender_id: sender_psid,
      page_fb_id: page_fb_id
    }
    , function (err, userState) {
      let cus_order = new CustomerOrder({
        bot_id: bot_id,
        cus_name: userState.cus_name,
        cus_email: userState.cus_email,
        cus_phone: userState.cus_phone,
        cus_address: userState.cus_address,
        goods_name: userState.goods_name,
        goods_id: userState.goods_id,
        goods_price: userState.goods_price,
        goods_amount: userState.goods_amount,
      })
      cus_order.save(function (err, _cus_order) {
        let gc = new GoodsConfirmation({
          bot_id: bot_id,
          user_id: user_id,
          customer_order: _cus_order._id,
        })
        gc.save((err, _gc) => {
          UserState.findOneAndDelete({
            page_fb_id: page_fb_id,
            sender_id: sender_psid
          }, function (err, res) {
            show_main_menu(sender_psid, page_fb_token, `ยืนยันการสั่งซื้อ\nหมายเลข order: ${_cus_order._id}\n`)
          })
        })
      })
    })
}

function delete_state(sender_psid, page_fb_id, page_fb_token, txt = null) {
  UserState.findOneAndDelete({
    sender_id: sender_psid,
    page_fb_id: page_fb_id
  }, function (err, res) {
    show_main_menu(sender_psid, page_fb_token, txt)
  })
}

function show_payment(sender_psid, page_fb_token, user_id) {
  Account.find({ user_id: user_id }, function (err, accounts) {
    let txt = "โปรดโอนเงินเข้าธนาคารดังต่อไปนี้\n"
    accounts.forEach(acc => {
      txt += `${acc.bank_name}\nหมายเลขบัญชี ${acc.account_id}\nชื่อบัญชี ${acc.account_name}\n------\n`
    });
    show_main_menu(sender_psid, page_fb_token, txt)
  })
}

function show_main_menu(sender_psid, page_fb_token, txt = null, quick_replies = null) {
  let main_menu = getMainMenu();
  sendQuickReply(
    sender_psid,
    txt ? txt : main_menu["title"],
    quick_replies ? quick_replies : main_menu["quick_replies"],
    page_fb_token
  );
}

function inform_payment(sender_psid, page_fb_id, page_fb_token) {
  if (updateState(page_fb_id, sender_psid, "INFORM_PAYMENT"))
    callSendAPI(
      sender_psid,
      {
        text: `โปรดระบุรหัสคำสั่งซื้อ`
      },
      page_fb_token
    );
}

function handleMessage(sender_psid, received_message, page_fb_id = null) {
  Bot.findOne(
    {
      page_fb_id: page_fb_id,
      state: "online"
    },
    function (err, bot) {
      if (bot && !err) {
        // Checks if the message contains text
        if (received_message.quick_reply) {
          let payload = received_message.quick_reply.payload
          switch (payload) {
            case "BUY_ORDER":
              updateState(page_fb_id, sender_psid, "STAGE_NEW")
              sendGenericMessage(sender_psid, bot.id, bot.page_fb_token)
              break;
            case "INFORM_PAYMENT":
              inform_payment(sender_psid, page_fb_id, bot.page_fb_token)
              break;
            case "SHOW_QUESTIONS":
              updateState(page_fb_id, sender_psid, "QUESTIONS")
              show_questions(sender_psid, page_fb_id, bot.page_fb_token, bot.user_id)
              break;
            case "SHOW_PAY_CHANNELS":
              updateState(page_fb_id, sender_psid, "STAGE_NEW")
              show_payment(sender_psid, bot.page_fb_token, bot.user_id)
              break;
            case "CANCEL_CUR_ORDER":
              delete_state(sender_psid, page_fb_id, bot.page_fb_token)
              break;
            case "CONFIRM_CUR_ORDER":
              confirm_cur_order(sender_psid, page_fb_id, bot.page_fb_token, bot._id, bot.user_id)
              break;
            case "EDIT_CUR_ORDER":
              sendQuickReply(
                sender_psid,
                "โปรดเลือกหัวข้อที่ต้องการแก้ไข",
                [
                  {
                    content_type: "text",
                    title: "ชื่อนามสกุล",
                    payload: "EDIT_CUR_NAME"
                  },
                  {
                    content_type: "text",
                    title: "สินค้า",
                    payload: "EDIT_CUR_GOODS"
                  },
                  {
                    content_type: "text",
                    title: "จำนวนสินค้า",
                    payload: "EDIT_CUR_GOODS_AMOUNT"
                  },
                  {
                    content_type: "text",
                    title: "โทรศัพท์",
                    payload: "EDIT_CUR_PHONE"
                  },
                  {
                    content_type: "text",
                    title: "อีเมล",
                    payload: "EDIT_CUR_EMAIL"
                  },
                  {
                    content_type: "text",
                    title: "ที่อยู่",
                    payload: "EDIT_CUR_ADDRESS"
                  },
                  {
                    content_type: "text",
                    title: "ยกเลิก",
                    payload: "CANCEL_CUR_EDIT"
                  },
                ],
                bot.page_fb_token
              );
              break;
            case "EDIT_CUR_NAME":
              updateState(page_fb_id, sender_psid, "EDIT_CUR_NAME")
              callSendAPI(
                sender_psid,
                {
                  text: "กรุณาระบุชื่อใหม่"
                },
                bot.page_fb_token
              );
              break;
            case "EDIT_CUR_GOODS":
              updateState(page_fb_id, sender_psid, "EDIT_CUR_GOODS")
              sendGenericMessage(sender_psid, bot.id, bot.page_fb_token);
              break;
            case "EDIT_CUR_GOODS_AMOUNT":
              updateState(page_fb_id, sender_psid, "EDIT_CUR_GOODS_AMOUNT")
              callSendAPI(
                sender_psid,
                {
                  text: "กรุณาระบุจำนวนสินค้าใหม่"
                },
                bot.page_fb_token
              );
              break;
            case "EDIT_CUR_EMAIL":
              updateState(page_fb_id, sender_psid, "EDIT_CUR_EMAIL")
              callSendAPI(
                sender_psid,
                {
                  text: "กรุณาระบุอีเมลใหม่"
                },
                bot.page_fb_token
              );
              break;
            case "EDIT_CUR_PHONE":
              updateState(page_fb_id, sender_psid, "EDIT_CUR_PHONE")
              callSendAPI(
                sender_psid,
                {
                  text: "กรุณาระบุเบอร์โทรใหม่"
                },
                bot.page_fb_token
              );
              break;
            case "EDIT_CUR_ADDRESS":
              updateState(page_fb_id, sender_psid, "EDIT_CUR_ADDRESS")
              callSendAPI(
                sender_psid,
                {
                  text: "กรุณาระบุที่อยู่ใหม่"
                },
                bot.page_fb_token
              );
              break;
            case "CANCEL_CUR_EDIT":
              send_pre_confirm(sender_psid, page_fb_id, bot.page_fb_token)
              //delete_state(sender_psid, page_fb_id, bot.page_fb_token, "คำสั่งซื้อถูกยกเลิก")
              break;
            default:
              updateState(page_fb_id, sender_psid, "STAGE_NEW")
              show_main_menu(sender_psid, bot.page_fb_token)
              break;
          }
        }
        else if (received_message.text) {
          // Create the payload for a basic text message, which
          // will be added to the body of our request to the Send API
          switch (received_message.text) {
            case "ยกเลิก":
              UserState.findOneAndDelete({
                sender_id: sender_psid,
                page_fb_id: page_fb_id
              }, (err, res) => { show_main_menu(sender_psid, bot.page_fb_token) })
              break;
            case "สั่งซื้อสินค้า":
              updateState(page_fb_id, sender_psid, "STAGE_NEW")
              sendGenericMessage(sender_psid, bot.id, bot.page_fb_token);
              break;
            case "แจ้งชำระเงิน":
              inform_payment(sender_psid, page_fb_id, bot.page_fb_token)
              break;
            case "วิธีชำระเงิน":
              updateState(page_fb_id, sender_psid, "STAGE_NEW")
              show_payment(sender_psid, bot.page_fb_token, bot.user_id)
              break;
            case "ตรวจสอบสินค้า":
              if (updateState(page_fb_id, sender_psid, "CHECK_USER_ORDER"))
                callSendAPI(
                  sender_psid,
                  {
                    text: "ขอรหัสการสั่งซื้อ"
                  },
                  bot.page_fb_token
                );
              break;
            default:
              UserState.findOneOrCreate({
                page_fb_id: page_fb_id,
                sender_id: sender_psid
              }, function (err, userState) {
                let state = userState.current_stage
                if (state.includes("ADD_AMOUNT_")) {
                  if (isNaN(received_message.text)) {
                    updateState(page_fb_id, sender_psid, state)
                    callSendAPI(
                      sender_psid,
                      {
                        text: `โปรดระบุจำนวนเป็นตัวเลขเท่านั้น`
                      },
                      bot.page_fb_token
                    );
                  }
                  else {
                    if (updateState(page_fb_id, sender_psid, "ADD_NAME", { goods_amount: parseInt(received_message.text) }))
                      callSendAPI(
                        sender_psid,
                        {
                          text: `โปรดระบุชื่อของคุณ`
                        },
                        bot.page_fb_token
                      );
                  }
                }
                else if (state.includes("QUESTIONS")) {
                  var query = Question.find({ user_id: bot.user_id }).skip(parseInt(received_message.text) - 1).limit(1)
                  query.exec(function (err, data) {
                    let text = "";
                    if (err || !Array.isArray(data) || !data.length) {
                      text = "ไม่พบคำตอบ"
                    }
                    else {
                      text = data[0].answer
                    }
                    delete_state(sender_psid, page_fb_id, bot.page_fb_token, text)
                  });
                }
                else if (state.includes("ADD_NAME")) {
                  if (updateState(page_fb_id, sender_psid, "EDIT_EMAIL", { cus_name: received_message.text }))
                    callSendAPI(
                      sender_psid,
                      {
                        text: `โปรดระบุอีเมล`
                      },
                      bot.page_fb_token
                    );

                }
                else if (state.includes("EDIT_EMAIL")) {
                  if (updateState(page_fb_id, sender_psid, "EDIT_PHONE", { cus_email: received_message.text }))
                    callSendAPI(
                      sender_psid,
                      {
                        text: `โปรดระบุเบอร์โทร`
                      },
                      bot.page_fb_token
                    );
                }
                else if (state.includes('EDIT_PHONE')) {
                  if (updateState(page_fb_id, sender_psid, "EDIT_ADDRESS", { cus_phone: received_message.text }))
                    callSendAPI(
                      sender_psid,
                      {
                        text: `โปรดระบุที่อยู่`
                      },
                      bot.page_fb_token
                    );
                }
                else if (state.includes("EDIT_ADDRESS")) {
                  send_pre_confirm(sender_psid, page_fb_id, bot.page_fb_token, { cus_address: received_message.text })
                }
                else if (state.includes("CONFIRM_ORDER")) {
                  confirm_cur_order(sender_psid, page_fb_id, bot.page_fb_token, bot._id, bot.user_id)
                }
                else if (state.includes("INFORM_PAYMENT")) {
                  CustomerOrder.findById(received_message.text, function (err, cus_orer) {
                    let txt = "ไม่พบหมายเลข Order ดังกล่าว กรุณาลองใหม่อีกครั้ง หรือพิมพ์ 'ยกเลิก'"
                    if (cus_orer) {
                      updateState(page_fb_id, sender_psid, `UPLOAD_EVIDENT_TRANSFER#${cus_orer._id}`)
                      txt = "โปรดอัพโหลดรูปภาพหลักฐานการโอน"
                      callSendAPI(
                        sender_psid,
                        {
                          text: txt
                        },
                        bot.page_fb_token
                      );
                    } else {
                      updateState(page_fb_id, sender_psid, state)
                      callSendAPI(
                        sender_psid,
                        {
                          text: txt
                        },
                        bot.page_fb_token
                      );
                    }
                  })
                }
                else if (state.includes("EDIT_CUR_NAME")) {
                  send_pre_confirm(sender_psid, page_fb_id, bot.page_fb_token, { cus_name: received_message.text })
                }
                else if (state.includes("EDIT_CUR_GOODS_AMOUNT")) {
                  send_pre_confirm(sender_psid, page_fb_id, bot.page_fb_token, { goods_amount: parseInt(received_message.text) })
                }
                else if (state.includes("EDIT_CUR_PHONE")) {
                  send_pre_confirm(sender_psid, page_fb_id, bot.page_fb_token, { cus_phone: received_message.text })
                }
                else if (state.includes("EDIT_CUR_EMAIL")) {
                  send_pre_confirm(sender_psid, page_fb_id, bot.page_fb_token, { cus_email: received_message.text })
                }
                else if (state.includes("EDIT_CUR_ADDRESS")) {
                  send_pre_confirm(sender_psid, page_fb_id, bot.page_fb_token, { cus_address: received_message.text })
                }
                else {
                  updateState(page_fb_id, sender_psid, `STATE_NEW`)
                  show_main_menu(sender_psid, bot.page_fb_token)
                }
              })
              break;
          }
        } else if (received_message.attachments) {
          // Get the URL of the message attachment
          let attachment_url = received_message.attachments[0].payload.url;
          UserState.findOneOrCreate({
            page_fb_id: page_fb_id,
            sender_id: sender_psid
          }, function (err, userState) {
            if (userState.current_stage.includes("UPLOAD_EVIDENT_TRANSFER#")) {
              GoodsConfirmation.findOneAndUpdate(
                {
                  customer_order: userState.current_stage.split("#")[1]
                },
                {
                  $set: { evidence_transfer_url: attachment_url }
                },
                {
                  useFindAndModify: false
                },
                (err, cor) => {
                  delete_state(sender_psid, page_fb_id, bot.page_fb_token, "ส่งหลักฐานเรียบร้อย โปรดรอร้านค้าติดต่อกลับ")
                }
              )
            }
          })

        }
      }
    }
  );

}

function send_pre_confirm(sender_psid, page_fb_id, page_fb_token, update_data = null) {
  updateState(page_fb_id, sender_psid, "CONFIRM_ORDER", update_data).then((res) => {
    UserState.findOneOrCreate({
      page_fb_id: page_fb_id,
      sender_id: sender_psid
    }, function (err, userState) {
      let text =
        `ชื่อ ${userState.cus_name}\n` +
        `อีเมล ${userState.cus_email}\n` +
        `เบอร์โทร ${userState.cus_phone}\n` +
        `ที่อยุ่ ${userState.cus_address}\n` +
        `--------\n` +
        `สินค้า ${userState.goods_name}\n` +
        `ราคา ${userState.goods_price}\n` +
        `จำนวน ${userState.goods_amount}\n` +
        `รวมเป็นเงิน ${userState.goods_price * userState.goods_amount} บาท`
      sendQuickReply(
        sender_psid,
        text,
        [
          {
            content_type: "text",
            title: "ยืนยันการสั่งซื้อ",
            payload: "CONFIRM_CUR_ORDER"
          },
          {
            content_type: "text",
            title: "แก้ไขข้อมูล",
            payload: "EDIT_CUR_ORDER"
          },
          {
            content_type: "text",
            title: "ยกเลิก",
            payload: "CANCEL_CUR_ORDER"
          },
        ]
        ,
        page_fb_token
      );
    })
  })
}

function show_questions(sender_psid, page_fb_id, page_fb_token, user_id) {
  Question.find({ user_id: user_id }, function (err, questions) {
    if (err || !Array.isArray(questions) || !questions.length) {
      return delete_state(sender_psid, page_fb_id, page_fb_token, "ไม่พบรายการคำถาม")
    }
    else {
      let text = `โปรดพิมพ์เลขด้านล่างเพื่อถามคำถาม หรือ พิมพ์ 'ยกเลิก' เพื่อกลับสู่เมนูหลัก\n`
      questions.forEach((q, i) => {
        text += `${i + 1} ${q.question} \n`
      });
      return callSendAPI(
        sender_psid,
        { text: text },
        page_fb_token
      )
    }
  })
}

function handlePostback(sender_psid, received_postback, page_fb_id) {
  // Get the payload for the postback
  Bot.findOne(
    {
      page_fb_id: page_fb_id,
      state: "online"
    },
    function (err, bot) {
      let payload = received_postback.payload;
      //console.log("received_postback", received_postback.payload)
      if (payload.includes("BUY_PRODUCT_")) {
        Goods.findById(payload.split("_")[2], function (err, goods) {
          UserState.findOneOrCreate({
            page_fb_id: page_fb_id,
            sender_id: sender_psid
          }, function (err, userState) {
            let state = userState.current_stage

            if (state.includes("EDIT_CUR_GOODS")) {
              send_pre_confirm(sender_psid, page_fb_id, bot.page_fb_token, {
                goods_id: goods._id,
                goods_name: goods.goods_name,
                goods_price: parseInt(goods.goods_price)
              })
            }
            else {
              if (updateState(page_fb_id, sender_psid, `ADD_AMOUNT_${goods._id}`, {
                goods_id: goods._id,
                goods_name: goods.goods_name,
                goods_price: parseInt(goods.goods_price)
              })) {
                callSendAPI(
                  sender_psid,
                  {
                    text: `โปรดระบุจำนวน`
                  },
                  bot.page_fb_token
                );
                console.log('Payload => ', payload)
              }
            }
          })
        })
      }
    }
  );
}

function callSendAPI(sender_psid, response, page_fb_token) {
  // Construct the message body
  let request_body = {
    recipient: {
      id: sender_psid
    },
    message: response
  };
  // Send the HTTP request to the Messenger Platform
  request(
    {
      uri: "https://graph.facebook.com/v2.6/me/messages",
      qs: {
        access_token: page_fb_token
      },
      method: "POST",
      json: request_body
    },
    (err, res, body) => {
      if (!err) {
      } else {
      }
    }
  );
}

function sendGenericMessage(sender_psid, bot_id, page_fb_token) {
  Bot.findOne({ _id: bot_id }, function (err, bot) {
    //console.log("bot here:", bot)
    Goods.find({ _id: bot.goods_list }, function (err, goods_list) {
      // console.log("good_list:", goods_list)
      if (err || !Array.isArray(goods_list) || !goods_list.length) {
        delete_state(sender_psid, bot.page_fb_id, page_fb_token, "ไม่พบสินค้าที่วางจำหน่าย")
        // return callSendAPI(
        //   sender_psid,
        //   {
        //     text: `ไม่พบสินค้าที่วางจำหน่าย`
        //   },
        //   page_fb_token
        // )
      }

      let elements = [];
      goods_list.forEach(goods => {
        elements.push({
          title: goods.goods_name,
          subtitle: `${goods.goods_description} \n ราคา ${goods.goods_price} บาท`,
          image_url: "https://combercobot.com/" + goods.goods_pic,
          buttons: [
            // {
            //   type: "postback",
            //   title: "ดูรายละเอียดสินค้า",
            //   payload: `SHOW_PRODUCT_${goods._id}`
            // },
            {
              type: "postback",
              title: "ซื้อสินค้า",
              payload: `BUY_PRODUCT_${goods._id}`
            }
          ]
        });
      });

      let messageData = {
        attachment: {
          type: "template",
          payload: {
            template_type: "generic",
            elements: elements
          }
        }
      };
      callSendAPI(sender_psid, messageData, page_fb_token);
    })
  })
}

function sendQuickReply(sender_psid, text, quick_replies, page_fb_token) {
  let messageData = {
    text: text,
    quick_replies: JSON.stringify(quick_replies)
  };
  callSendAPI(sender_psid, messageData, page_fb_token);
}

module.exports = router;
