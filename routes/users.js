var express = require("express");
//var multer = require('multer');
//var upload = multer()
var router = express.Router();
var auth = require("../middlewares/auth.js");
const User = require("../models/user.model");
const mongoose = require("mongoose");
var moment = require('moment');
var is_role = require("../middlewares/check_role");

router.post("/login", login);
router.get("/showlogin",showlogin);

router.use(function(req, res, next) {
  next();
});

router.use([auth()]);
router.get("/logout", [auth()], logout);
router.get("/profile/:id", [auth()],index);
router.get("/edit/:id", [is_role('admin')],edit);
router.post("/edit/:id", [is_role('admin')],put);
// router.get("/delete/:id", deleteuser);
redirect_err = "/error",
redirect_ok = "/"

function login(req, res, next) {
  if (!req.body.username || !req.body.password) {
    res.send("login failed");
  }
  if (req.session && req.session.user) {
    res.redirect("/");
  }

  User.findOne(
    {
      username: req.body.username,
      password: req.body.password
    },
    "_id username",
    function(err, user) {
      if (err) return handleError(err);
      if (user != null) {
        req.session.user = user._id;
        res.redirect("/");
      } else {
        res.send("ไม่เจอนะครับ");
      }
    }
  );
}

function logout(req, res, next) {
  req.session.destroy();
  res.redirect("../users/showlogin");
}

function showlogin(req, res, next) {
  res.render("form/login", {
    title: "Login"
  });
}

const findUser = async function (params) { 
  return await User.findOne(params)  
}
async function getalluser() {
  return await User.find({})
}

function index(req, res) {
  var query = {
    _id: mongoose.Types.ObjectId(req.session.passport.user)
  };
  // console.log("myrole: ",myrole)
  console.log("query : ",query)
  let myuser = findUser(query);
  myuser.then(function(result) {
    // console.log("THis is myrole",result)
    if (result.role === 'admin'){
      console.log("get all user")
      result = getalluser();
      result.then(function(myresult){
        // console.log("All user: ",myresult)
        res.render("pages/user", {
          title: "Profile",
          user: myresult,
          moment: moment
        });
      })
    }
    else{
      console.log("user normal")
      res.render("pages/profile", {
        title: "Profile",
        user: result,
        moment: moment
      });
    }
 })
  return
}
// edit question [view]
function edit(req, res) {
  User.findOne({_id:req.params.id}, function (err,result) {
    if (err || !result) res.redirect(redirect_err);
    res.render("edit/edit_reg_user", {
      title: "Edit User",
      user: result
    });
  });
}

// edit question
function put(req, res) {
  User.updateOne({_id:req.params.id}, 
    { 
      $set: {
        role: req.body.role,
      },
    }, function (err) {
    if (err) res.redirect(redirect_err);
    res.redirect(redirect_ok);
  });
}

module.exports = router;
