var express = require("express");
var router = express.Router();
var moment = require('moment');

// middlewares
var auth = require("../middlewares/auth.js");

// helpers
var get_user = require("../helpers/getUserInfo");

const mongoose = require("mongoose");
const GoodsConfirmation = require("../models/goods_confirmation.model"),
  redirect_err = "/error",
  redirect_ok = "/goods_confirmations/"
const User = require("../models/user.model");
const Order = require("../models/customer_order.model");

// all routes middlewares
router.use([auth()]);

// routes
router.get("/", index);
router.get("/details/:id", details)
router.get("/edit/:id", edit)
router.post("/edit/:id", put)


const findUser = async function (params) { 
  return await User.findOne(params)  
}

const findGoodsConfirmation = async function (params) { 
  return await GoodsConfirmation.findOne(params)  
}

const findOrder = async function (params) { 
  return await Order.findOne(params)  
}

async function joinallorder(){
    return await GoodsConfirmation.aggregate([
      {
        $lookup:
        {
          from: 'bots',
          localField: 'bot_id',
          foreignField: '_id',
          as: 'botdata'        
        }
      },
      {
        $lookup:
        {
          from: 'customer_orders',
          localField: 'customer_order',
          foreignField: '_id',
          as: 'orderdata'         
        }
      },
    ])
}
async function joinuserorder(params){
    return await GoodsConfirmation.aggregate([
        {
            $match: params
        }, 
        {           
          $lookup:
          {
            from: 'bots',
            localField: 'bot_id',
            foreignField: '_id',
            as: 'botdata'          
          }
        },
        {
          $lookup:
          {
            from: 'customer_orders',
            localField: 'customer_order',
            foreignField: '_id',
            as: 'orderdata'          
          }
        },
    ])
}

// show all goods confirmations [view]
function index(req, res) {
  var query = {
    _id: mongoose.Types.ObjectId(req.session.passport.user)
  };
  // console.log("query : ",query)
  let myuser = findUser(query);
  
  myuser.then(function(result) {
    if (result.role == 'admin'){
        console.log("get all order")
        result = joinallorder();
        result.then(function(myresult){
        console.log("All order: ",myresult)    
        // console.log("All order: ",myresult[0].orderdata)                
            res.render("pages/goods_confirmation", {
                title: "Order List",
                order: myresult,
                moment:moment
            });
        })
    }
    else{
        var orderquery = {
            user_id: mongoose.Types.ObjectId(req.session.passport.user)
        };
        result = joinuserorder(orderquery);
        result.then(function(myresult){
          // console.log("order:",myresult)
          // console.log("order:",myresult[0].orderdata)
          // console.log("order:",myresult[0].botdata)
            res.render("pages/goods_confirmation", {
                title: "Order",
                order: myresult,
                moment:moment
            });                   
        })        
    }
 });
  return
}

function details(req, res) {
  var query = {
    _id: req.params.id
  };
  let myorder = findOrder(query);
  
  myorder.then(function(result) {
    res.render("pages/goods_detail", {
        title: "Order Detail",
        detail: result,
        moment:moment
    });                
  })
}

// edit goods confirmations [view]
function edit(req, res) {
  var query = {
    _id: req.params.id
  };
  // console.log("query : ",query)
  let myconfirmation = findGoodsConfirmation(query);
  myconfirmation.then(function(myresult){
    res.render("edit/edit_reg_goods_confirmation", {
      title: "Edit Order",
      order: myresult,
    });                   
  })   
}

// put goods confirmations
function put(req, res) {
 
  GoodsConfirmation.updateOne({_id:req.params.id}, 
    { 
      $set: {
        status: req.body.status,
      },
    }, function (err) {
    if (err) res.redirect(redirect_err);
    res.redirect(redirect_ok);
  });
}
module.exports = router;
