var express = require("express");
var router = express.Router();

// middlewares
var auth = require("../middlewares/auth");

// helpers
var is_role = require("../middlewares/check_role");

const mongoose = require("mongoose");
const Package = require("../models/package.model"),
  redirect_err = "/error",
  redirect_ok = "/package/"
const User = require("../models/user.model");

router.use([auth()]);

// routes
router.get("/", index);

router.use([is_role('admin')]);
router.get("/create", create);
router.post("/create", store);
router.get("/edit/:id", edit)
router.post("/edit/:id", put)
router.get("/delete/:id", remove);

// MongoDB Query
const findUser = async function (params) { 
  return await User.findOne(params)  
}
async function getallpackage() {
  return await Package.find({})
}

// show all packages [view]
function index(req, res) {
  var query = {
    _id: mongoose.Types.ObjectId(req.session.passport.user)
  };
  // console.log("query : ",query)
  let myuser = findUser(query);
  myuser.then(function(result) {
    if (result.role == 'admin'){
      console.log("get all user")
      result = getallpackage();
      result.then(function(myresult){
        console.log("All user: ",myresult)
        res.render("pages/package", {
          title: "Package List",
          package: myresult,
        });
        // console.log(package)
      })
    }
    else{
      res.redirect(redirect_err)
    }
 })
  return
}


// create a new package [view]
function create(req, res) {
  res.render("form/reg_package", {
    title: "Create a new package"
  });
}

// store a new package
function store(req, res) {
  // let user = get_user();
  Package.create(Object.assign(req.body, { 
        package_name: req.body.name,
        package_term:req.body.term,
        price:req.body.price,
        status: req.body.status}),function (err) {
    if (err) res.redirect(redirect_err)
    res.redirect(redirect_ok);
  });
}

// edit package [view]
function edit(req, res) {
  Package.findOne({_id:req.params.id}, function (err,result) {
    if (err || !result) res.redirect(redirect_err);
    console.log("package:",result)
    res.render("edit/edit_reg_package", {
      title: "Edit Package",
      package: result
    });
  });
}

// edit package
function put(req, res) {
  Package.updateOne({_id:req.params.id}, 
    { 
      $set: {
        package_name: req.body.name,
        package_term:req.body.term,
        price:req.body.price,
        status: req.body.status
      },
    }, function (err) {
    if (err) res.redirect(redirect_err);
    res.redirect(redirect_ok);
  });
}

function remove(req, res) {
  var query = {
    _id: mongoose.Types.ObjectId(req.params.id)
  };
  Package.deleteOne(query, function(err) {
    if (err) res.redirect(redirect_err);
    res.redirect(redirect_ok);
  });
}


module.exports = router;


