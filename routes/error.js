var express = require("express");
var router = express.Router();

router.get("/", index);

function index(req, res) {    
    res.render("e_error", {
        title: "Error",
    });
    return
}
module.exports = router;