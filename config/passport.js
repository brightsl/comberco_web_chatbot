// var LocalStrategy    = require('passport-local').Strategy;
var FacebookStrategy = require("passport-facebook").Strategy;
// var FacebookTokenStrategy = require('passport-facebook-token');


// load up the user model
var User = require("../models/user.model");

// load the auth variables
var configAuth = require("./auth");

module.exports = function (passport) {
  // used to serialize the user for the session
  passport.serializeUser(function (user, done) {
    done(null, user.id);
  });

  // used to deserialize the user
  passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
      done(err, user);
    });
  });

  // FACEBOOK
  passport.use(
    new FacebookStrategy(
      {
        // pull in our app id and secret from our auth.js file
        clientID: configAuth.facebookAuth.clientID,
        clientSecret: configAuth.facebookAuth.clientSecret,
        callbackURL: configAuth.facebookAuth.callbackURL,
        profileURL: configAuth.facebookAuth.profileURL,
        profileFields: configAuth.facebookAuth.profileFields,
        enableProof: true
      },
    // passport.use(new FacebookTokenStrategy({
    //   clientID: configAuth.facebookAuth.clientID,
    //   clientSecret: configAuth.facebookAuth.clientSecret,
    //   callbackURL: configAuth.facebookAuth.callbackURL,
    //   profileURL: configAuth.facebookAuth.profileURL,
    //   profileFields: configAuth.facebookAuth.profileFields,
    //   fbGraphVersion: 'v3.0',      
    // },

      // facebook will send back the token and profile
      // function (accessToken, refreshToken, profile, done) {
      function (token, refreshToken, profile, done) {
        // asynchronous

        // console.log("my profile", profile)
        // console.log("fb token " + token);
        process.nextTick(function () {
          // find the user in the database based on their facebook id
          User.findOne({ fb_id: profile.id }, function (err, user) {
            // if there is an error, stop everything and return that
            // ie an error connecting to the database
            if (err) return done(err);

            // console.log("user found:",user)
            // if the user is found, then log them in
            if (user) {
              return done(null, user); // user found, return that user
            } else {
              // if there is no user found with that facebook id, create them

              var newUser = new User();

              // set all of the facebook information in our user model
              newUser.fb_id = profile.id; // set the users facebook id
              newUser.fb_token = token; // we will save the token that facebook provides to the user
              newUser.fb_name = profile.displayName; // look at the passport user profile to see how names are returned
              newUser.email = (profile.emails[0].value || '').toLowerCase(); // facebook can return multiple emails so we'll take the first
              // console.log("newuser:",newUser)

              // save our user to the database
              newUser.save(function (err) {
                if (err) throw err;

                // if successful, return the new user
                return done(null, newUser);
              });
            }
          });
        });
      }
    )
  );
};
