module.exports = {

    'facebookAuth': {
        'clientID': '2165162846865827', // your App ID
        'clientSecret': 'ad22b8a99ca448818a4fb32ccd7d5426', // your App Secret
        'callbackURL': 'https://combercobot.com/auth/facebook/callback',
        // 'callbackURL': 'http://localhost/auth/facebook/callback',
        'profileURL': 'https://graph.facebook.com/v3.3/me?fields=first_name,last_name,email,displayName',
        'profileFields': ['id', 'email', 'name',"displayName"] // For requesting permissions from Facebook API
    }
    //'callbackURL': 'http://localhost:3000/auth/facebook/callback',
    // 'twitterAuth' : {
    //     'consumerKey'       : 'your-consumer-key-here',
    //     'consumerSecret'    : 'your-client-secret-here',
    //     'callbackURL'       : 'http://localhost:8080/auth/twitter/callback'
    // },

    // 'googleAuth' : {
    //     'clientID'      : 'your-secret-clientID-here',
    //     'clientSecret'  : 'your-client-secret-here',
    //     'callbackURL'   : 'http://localhost:8080/auth/google/callback'
    // }

};
