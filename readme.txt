Worknote :
-----------Main Function -----------------
1.User
    -CRUD Finished
2.bots
3.goods
    -CRUD Finished
4.goods confirmation
5.Payment
    - C,R Finished
    - U (edit function)
6.Account
    -CRUD finished
7.Package
    -CRUD Finished
8.Bank
    -CRUD Finished
    *note input bank logo with image,save to path
9.Question
    

----------------- Optional -------------------
*alert before delete and edit all projects
*input verify field


edit_bot.ejs (additional)
    <span class="section">จัดการบัญชีของ bot</span> 
    <table class="table table-hovered">
        <thead class="thead-dark">
            <tr>
                <th>รายการ</th>
                <th>ข้อมูล</th>
            </tr>
        </thead>
        <tbody>      
            <% bot.account_channels.forEach((account, index) => { %>                              
                <tr>
                    <td><%=index+1 %> ชื่อสินค้า </td>    
                    <td><%=account.account_name%></td>                         
                </tr> 
                <tr>
                    <td></td>
                    <td><%=account.account_id%></td>                                         
                </tr>
                <tr>
                    <td></td>
                    <td><%=account.bank_name%></td> 
                </tr>
                <tr>
                    <td colspan="2" class="text-center"><a href="/bot/delete/<%=bot._id%>/1/<%=account._id%>">ลบบัญชีนี้</a></td>                                            
                </tr>
            <% }) %>                                                            
        </tbody>
    </table>
    <form action="" class="form-horizontal form-label-left post-form" novalidate method="POST">                   
        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Facebook page <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="1" name="id" placeholder="ID" required="required" type="text" value="<%=bot.page_fb_name%>" readonly>
            </div>
        </div>
        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">เลือกบัญชี <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="account_id">
                    <% account.forEach((account, index) => { %>                             
                        <option value="<%=account._id%>"><%=account.bank_name%>  <%=account.account_id%> <%=account.account_name%> </option>                                                             
                    <% }) %>                                                                   
                </select>
            </div>
        </div>                           
        <div class="form-group">
            <div class="col-md-6 col-md-offset-3">                    
            <button id="send" type="submit" class="btn btn-success">Submit</button>
            </div>
        </div>
    </form> 
