const constants = require("../constants/constants");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
mongoose.set('useFindAndModify', false);

let BotSchema = new Schema(
  {
    user_id: {
      type: mongoose.Schema.Types.ObjectId,ref: "Users",
      required: true
    },
    package_id: {
      type: mongoose.Schema.Types.ObjectId,
      required: true
    },
    type: {
      type: String,
      required: true,
      default: constants.FB_BOT_TYPE
    },
    page_fb_token: {
      type: String
    },
    page_fb_id: {
      type: Number
    },
    page_fb_name: {
      type: String
    },
    goods_list: [{type: mongoose.Schema.Types.ObjectId, ref: "Goods"}],
    // account_channels: [mongoose.Schema.Types.ObjectId],
    account_channels: [{type: mongoose.Schema.Types.ObjectId, ref: "Accounts"}],
    state: {
      type: String,
      default: "online" //offline
    },
    free_trial: {
      type: Boolean
    },
    start_date: {
      type: Date
    },
    end_date: {
      type: Date
    }
  },
  { timestamps: true, collection: "bots" }
);

BotSchema.statics.findAndModify = function (query, sort, doc, options, callback) {
  return this.collection.findAndModify(query, sort, doc, options, callback);
};

module.exports = mongoose.model("Bots", BotSchema);
