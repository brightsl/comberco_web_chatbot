const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let PaymentSchema = new Schema(
  {
    user_id: {
      type: mongoose.Schema.Types.ObjectId,
      required: true
    },
    bot_id: {
      type: mongoose.Schema.Types.ObjectId, ref: "Bots",
      required: true
    },
    package_id: {
      type: mongoose.Schema.Types.ObjectId, ref: "Packages",
      required: true
    },
    status: {
      type: String,
      required: true,
      default: "waiting_for_payment" // waiting_for_checking, // confirmed
    },
    evidence_transfer: {
      type: String,
      // required: true
    },
    confirmedAt: {
      type: Date,
      // required: true
    }
  },
  {
    timestamps: true,
    collection: "payments"
  }
);

module.exports = mongoose.model("Payments", PaymentSchema);
