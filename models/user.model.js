const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let UserSchema = new Schema(
  {
    fb_id: {
      type: String,
      required: false,
      max: 255
    },
    fb_token: {
      type: String,
      required: true,
      max: 255
    },
    fb_name: {
      type: String,
      required: true,
      max: 255
    },
    email: {
      type: String,
      // required: true
    },
    phone_number: {
      type: String
    },
    line_id: {
      type: String
    },
    free_trials: {
      type: Boolean,
      default: false
    },
    role: {
      type: String,
      default: "user" //admin
    }
  },
  {
    timestamps: true,
    collection: "users"
  }
);

// Export the model
module.exports = mongoose.model("Users", UserSchema);
