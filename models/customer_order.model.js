const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let CustomerOrderSchema = new Schema(
  {
    bot_id: {
      type: mongoose.Schema.Types.ObjectId,
      required: true
    },
    goods_id: {
      type: mongoose.Schema.Types.ObjectId,
      required: true
    },
    goods_name: {
      type: String
    },
    goods_price: {
      type: Number
    },
    goods_amount: {
      type: Number
    },
    cus_name: {
      type: String
    },
    cus_phone: {
      type: String
    },
    cus_email: {
      type: String
    },
    cus_address: {
      type: String
    },
  },
  { timestamps: true, collection: "customer_orders" }
);

module.exports = mongoose.model("CustomerOrders", CustomerOrderSchema);
