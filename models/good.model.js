const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let GoodsSchema = new Schema(
  {
    user_id: {
      type: mongoose.Schema.Types.ObjectId,ref: "Users",
      required: true
    },
    // bot_id: {
    //   type: mongoose.Schema.Types.ObjectId,
    //   required: true
    // },
    goods_name: {
      type: String,
      required: true
    },
    goods_description: {
      type: String
    },
    goods_price: {
      type: Number,
      required: true
    },
    goods_pic: {
      type: String
    }
  },
  {
    timestamps: true,
    collection: "goods"
  }
);

module.exports = mongoose.model("Goods", GoodsSchema);
