const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let BankSchema = new Schema(
  {
    bank_name: {
      type: String,
      required: true
    },
    bank_logo: {
      type: String
    }
  },
  { timestamps: true, collection: "banks" }
);

module.exports = mongoose.model("Banks", BankSchema);
