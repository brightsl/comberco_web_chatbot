const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let AccountSchema = new Schema(
  {
    user_id: {
      type: mongoose.Schema.Types.ObjectId,ref: "Users",
      required: true
    },
    bank_name: {
      type: String,
      required: true,
    },
    account_id: {
      type: String,
      required: true
    },
    account_name: {
      type: String,
      required: true
    }
  },
  { timestamps: true, collection: "accounts" }
);

module.exports = mongoose.model("Accounts", AccountSchema);
