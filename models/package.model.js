const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let PackageSchema = new Schema(
  {
    package_name: {
      type: String,
      required: true
    },
    package_term: {
      type: Number,
      required: true
    },
    price: {
      type: Number,
      required: true
    },
    status: {
      type: String
    }
  },
  {
    timestamps: true,
    collection: "packages"
  }
);

module.exports = mongoose.model("Packages", PackageSchema);
