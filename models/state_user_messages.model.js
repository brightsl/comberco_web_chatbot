require("../constants/constants");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let StateUserMessageSchema = new Schema(
  {
    sender_id: {
      type: mongoose.Schema.Types.Mixed,
      required: true
    },
    page_fb_id: {
      type: mongoose.Schema.Types.Mixed,
      required: true
    },
    current_stage: {
      type: String,
      default: "START_STAGE",
      required: true
    },
    goods_id: {
      type: mongoose.Schema.Types.ObjectId
    },
    goods_name: {
      type: String
    },
    goods_price: {
      type: Number
    },
    goods_amount: {
      type: Number
    },
    cus_name: {
      type: String
    },
    cus_phone: {
      type: String
    },
    cus_email: {
      type: String
    },
    cus_address: {
      type: String
    },
    cur_time: {
      type: Number,
      default: Date.now()
    }
  }, {
    timestamps: true
  }
);

StateUserMessageSchema.statics.findOneOrCreate = function findOneOrCreate(condition, callback) {
  const self = this
  self.findOne(condition, (err, result) => {
    return result ? callback(err, result) : self.create(condition, (err, result) => { return callback(err, result) })
  })
}

module.exports = mongoose.model("StateUserMessages", StateUserMessageSchema);
