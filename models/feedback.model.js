const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let FeedbackSchema = new Schema(
  {
    user_id: {
      type: mongoose.Schema.Types.ObjectId,ref: "Users",
      required: true
    },
    feedback: {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: true
    },
    status: {
      type: String,
    },
  },
  {
    timestamps: true,
    collection: "feedbacks"
  }
);

module.exports = mongoose.model("Feedback", FeedbackSchema);
