const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let QuestionSchema = new Schema(
  {
    user_id: {
        type: mongoose.Schema.Types.ObjectId,ref: "Users",
        required: true
    },
    question: {
      type: String,
      required: true
    },
    answer: {
      type: String,
      required: true
    }
  },
  { timestamps: true, collection: "questions" }
);

module.exports = mongoose.model("Questions", QuestionSchema);
