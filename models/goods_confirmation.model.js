const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let GoodsConfirmationSchema = new Schema(
  {
    bot_id: {
      type: mongoose.Schema.Types.ObjectId,
      required: true
    },
    user_id: {
      type: mongoose.Schema.Types.ObjectId,
      required: true
    },
    // goods_name: {
    //   type: String,
    //   required: true
    // },
    // goods_price: {
    //   type: Number,
    //   required: true
    // },
    // user_id: {
    //   type: mongoose.Schema.Types.ObjectId,
    //   required: true
    // },
    // goods_id: {
    //   type: mongoose.Schema.Types.ObjectId,
    //   required: true
    // },
    customer_order: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "CustomerOrders"
    },
    evidence_transfer_url: {
      type: String
    },
    status: {
      type: String,
      required: true,
      default: "open"
    }
  },
  {
    timestamps: true,
    collection: "goods_confirmations"
  }
);

module.exports = mongoose.model("GoodsConfirmations", GoodsConfirmationSchema);
