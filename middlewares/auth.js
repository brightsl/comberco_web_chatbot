const User = require("../models/user.model")
const mongoose = require("mongoose");
const findUser = async function (params) { 
  return await User.findOne(params)  
}
module.exports = function(options, passport, app) {
  return function(req, res, next) {
    if (req.session && req.session.passport && req.session.passport.user) {
      res.locals.myuser = req.session.passport.user;     
      let myrole = findUser({_id: mongoose.Types.ObjectId(res.locals.myuser)});
      myrole.then(function(result) {
        // console.log("the result :",result)
        if (result.role === 'admin'){
          // console.log('admin')
          res.locals.myrole = 'admin'
          res.locals.displayname = result.fb_name
          return next();
        }
        else{
          // console.log("user")
          res.locals.myrole = 'user'
          res.locals.displayname = result.fb_name
          return next();
        }
      });
      // console.log("myuser:",res.locals.myuser)
      // console.log("myrole:",res.locals.myrole)
      
    } else {
      return res.redirect("../users/showlogin");
    }
  };
};
