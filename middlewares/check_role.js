const User = require("../models/user.model")
module.exports = function (options, passport, app) {
  return function (req, res, next) {
    if (req.session && req.session.passport && req.session.passport.user) {
      User.findById(req.session.passport.user, function (err, user) {
        if (err || !user) res.redirect("../users/showlogin")
        if (user.role === options) return next();
      })
    } else {
      //return res.status(401).send("unauthorized");
      return res.redirect("../users/showlogin");
    }
  };
};
