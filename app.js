var createError = require('http-errors');
var express = require('express');
var app = express();
var passport = require('passport');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser')
var logger = require('morgan');
var request = require('request');
var session = require('express-session');
var mongoose = require('mongoose');
var fs = require('fs');
// var methodOverride = require('method-override')
// var upload = require("express-fileupload");


//route
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var paymentRouter = require('./routes/payment');
var packageRouter = require('./routes/package');
var feedbackRouter = require('./routes/feedback');
// var bankRouter = require('./routes/bank');


// var receiptRouter = require('./routes/receipt');
//var serviceRouter = require('./routes/service');
var chatbotRouter = require('./routes/bot');
var webhookRouter = require('./routes/webhook');
var goodsConfirmmation = require('./routes/goods_confirmation');
var goodsRouter = require('./routes/goods');
var questionRouter = require('./routes/question');
var accountRouter = require('./routes/account');
var errorRouter = require('./routes/error');



//cron-job
var cron = require('node-cron');
var cronjob = require("./helpers/cronjob");
cron.schedule('0 23 * * *', function(){
  console.log('running terminate bot on 11pm everyday');
  cronjob()
});
// cron.schedule('5 * * *', function(){
//   console.log('running update service data on 5am everyday');
// });
// cron.schedule('* * * * *', function(){
//   console.log('running terminate bot on every minute');
//   cronjob()
// });

app.use(passport.initialize());
app.use(passport.session());

// const ejsLint = require('ejs-lint');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('ejs', require('express-ejs-extend'));

//Set up mongoose connection
let dev_db_url = 'mongodb://admin:myadminpassword@combercobot.com:27017/comberco_webchat?authSource=admin';

const mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB, {
  useNewUrlParser: true
});
mongoose.Promise = global.Promise;
const db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error:'));
global.db = db

app.use(logger('dev'));
// app.use(logger('combined'));
// app.use(methodOverride('X-HTTP-Method-Override'));
// app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
// app.use('/bank_logo', express.static('bank_logo'));
app.use('/goods_pic', express.static('goods_pic'));
app.use('/payment_evidence', express.static('payment_evidence'));
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
//app.use(express.static(__dirname, { dotfiles: 'allow' } ));
app.use(session({
  secret: 'f8bUqdfuCjHwhQszEOR0',
  resave: true,
  saveUninitialized: true
}));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/package', packageRouter);
// app.use('/bank', bankRouter);
app.use('/payment', paymentRouter);
app.use('/feedback', feedbackRouter);
// app.use('/receipt', receiptRouter);
//app.use('/service', serviceRouter);
app.use('/bot', chatbotRouter);
app.use('/webhook', webhookRouter);
app.use('/goodsConfirmation', goodsConfirmmation);
app.use('/goods', goodsRouter);
app.use('/question',questionRouter)
app.use('/account', accountRouter);
app.use('/error', errorRouter);

// app.use(upload());

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
// app.use(function(req, res, next) {
//   res.locals.myuser = req.session.passport.user;
//   next();
// });

module.exports = app;
